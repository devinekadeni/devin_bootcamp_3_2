<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class classClass extends Model
{
    public $primaryKey = 'class_id';
    protected $table = 'class';
    public $timestamps = false;
}
