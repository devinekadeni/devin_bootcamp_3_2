<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transactionClass extends Model
{
    public $primaryKey = 'transaction_id';
    protected $table = 'transaction';
    public $timestamps = false;
}
