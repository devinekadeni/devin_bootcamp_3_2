<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class roomClass extends Model
{
    public $primaryKey = 'room_id';
    protected $table = 'room';
    public $timestamps = false;
}
