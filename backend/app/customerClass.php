<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerClass extends Model
{
    public $primaryKey = 'customer_id';
    protected $table = 'customer';
    public $timestamps = false;
}
