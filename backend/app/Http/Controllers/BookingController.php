<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\roomClass;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    function bookingRoom(Request $request){
        $id = $request->input('room_id');
        $cust = $request->input('customer_id');

        DB::beginTransaction();
        try{
            roomClass::where('room_id','=',$id)->update(['customer_id' => $cust]);
            DB::commit();
            return response()->json(200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json($e->getMessage(),500);
        }
    }
}
