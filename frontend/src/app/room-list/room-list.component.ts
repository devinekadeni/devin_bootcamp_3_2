import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  roomList:Object[] = [
    {"room_id":408,"class_id":1,"floor_location":8,"available":true},
    {"room_id":816,"class_id":3,"floor_location":23,"available":true},
    {"room_id":621,"class_id":1,"floor_location":12,"available":false},
    {"room_id":1426,"class_id":2,"floor_location":47,"available":false},
    {"room_id":1215,"class_id":2,"floor_location":39,"available":true}
  ]

  book($index){
    this.roomList[$index]["available"] == true;
  }
}
