import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoomListComponent } from './room-list/room-list.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CustomerlistComponent } from './customerlist/customerlist.component';
import { PaymentBillComponent } from './payment-bill/payment-bill.component';
import { EditroomComponent } from './editroom/editroom.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomListComponent,
    CheckoutComponent,
    CustomerlistComponent,
    PaymentBillComponent,
    EditroomComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
